(function () {
    var pageNum = 1;
    var pdfDoc = null;
    var scale = 1;
    var pageRendering = false,
        pageNumPending = null;
    var checkprocess = true;
    var rotation = 0;
    var process = 1;
//This is where you start
    window.initPDFViewer = function(pdfURL) {
        pdfjsLib.getDocument(pdfURL).promise.then(function (pdf) {
            pdfDoc = pdf;

            //How many pages it has
            numPages = pdf.numPages;

            document.getElementById('totalpage').innerHTML = numPages;
            renderPage(pageNum);
            //Start with first page
            // for(var num = 1; num <= numPages; num++)
            // pdf.getPage(pageNum).then(handlePages);
            document.getElementById('zoomIn').addEventListener('click', zoomIn);
            document.getElementById('zoomOut').addEventListener('click', zoomOut);
            document.getElementById('clockwise').addEventListener('click', rotationClockwise);
            document.getElementById('anticlockwise').addEventListener('click', rotationAnticlockwise);
        });
    };

    /**
     * Get page info from document, resize canvas accordingly, and render page.
     * @param num Page number.
     */
    function renderPage(num) {
        pageRendering = true;
        // Using promise to fetch the page
        pdfDoc.getPage(num).then(page => {
            var viewport = page.getViewport({
                scale: scale, rotation: rotation
            });
            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);

            // Wait for rendering to finish
            renderTask.promise.then(function () {
                pageRendering = false;
                if (pageNumPending !== null) {
                    // New page rendering is pending
                    renderPage(pageNumPending);
                    pageNumPending = null;
                }
            });
        });

        // Update page counters
        document.getElementById('page_num').value = num;
        document.getElementById('process').textContent = process;
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num;
        } else {
            renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    function onPrevPage() {
        if (pageNum <= 1) {
            return;
        }
        pageNum--;
        queueRenderPage(pageNum);
    }

    document.getElementById('prev').addEventListener('click', onPrevPage);

    /**
     * Displays next page.
     */
    function onNextPage() {
        if (pageNum >= pdfDoc.numPages) {
            return;
        }
        if (pageNum == process){
            process++;
        }
        pageNum++;
        queueRenderPage(pageNum);
    }

    document.getElementById('next').addEventListener('click', onNextPage);

    /**
     * Asynchronously downloads PDF.
     */
    function zoomIn() {
        if (pdfDoc == null) {
            return;
        }
        scale = scale + 0.5;
        renderPage(pageNum);
    }

    function zoomOut() {
        if (pdfDoc == null) {
            return;
        }
        scale = scale - 0.5;
        renderPage(pageNum);
    }

    function rotationClockwise() {
        if (pdfDoc == null) {
            return;
        }
        rotation = rotation + 90;
        renderPage(pageNum);
    }

    function rotationAnticlockwise() {
        if (pdfDoc == null) {
            return;
        }
        rotation = rotation - 90;
        renderPage(pageNum);
    }
})();